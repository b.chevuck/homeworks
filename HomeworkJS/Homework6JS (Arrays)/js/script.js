/* Опишите своими словами как работает цикл forEach.

 Выполняет указанную функцию для каждого элемента массива
*/





console.log(filterBy(["hello", "world", 23, "23", null], 'string'));
console.log(filterBy(["hello", "world", 23, "23", null], 'number'));
console.log(filterBy(["hello", "world", 23, "23", null], 'object'));



function filterBy(array, dataType) {
 const filterArr = [];

    array.forEach(function (item) {
        if (typeof item !== dataType){
            filterArr.push(item);
        }
    });
    return filterArr;

}


