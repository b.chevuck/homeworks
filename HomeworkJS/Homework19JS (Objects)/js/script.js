function calculateProgressWorkers(workers, tasks, deadline) {

    let sumPointsTasks = 0;
    let sumPointsWorkers = 0;
    const WORKTIME = 8;


    for (let i = 0; i < workers.length; i++) {
        sumPointsWorkers += workers[i];
    }

    for (let i = 0; i < tasks.length; i++) {
        sumPointsTasks += tasks[i];
    }
    console.log("Workers can make PointsTasks at day (8hours): " + sumPointsWorkers);
    console.log("PointsTasks need to make: " + sumPointsTasks);


    let needWorkTime = (sumPointsTasks / sumPointsWorkers) * WORKTIME;
    console.log("Workers need time to end all PointsTasks: " + Math.ceil(needWorkTime) + " hours");

    let today = new Date();

    console.log("Today: " + today);
    console.log("Deadline: " + deadline);

    let deadlineFull = deadline.setDate(deadline.getDate() + 1);

    let businessDays = 0;
    for(let i = today; i <= deadlineFull; i = new Date(i.setDate(i.getDate() + 1) )) {
        if (i.getDay() !== 0 && i.getDay() !== 6){
            businessDays++;

        }
    }
   if (today.getHours() >= 17){
        businessDays--;
    }
    console.log("We have by deadline " + businessDays + " business days");

    let haveWorkTime = businessDays * WORKTIME;


    if (today.getHours()>= 9 && today.getHours() <= 17) {
        haveWorkTime -=  WORKTIME - (17-today.getHours());
    }
    let checkDeadline = haveWorkTime - needWorkTime;

    if (checkDeadline >= 0){
        let day = (haveWorkTime - needWorkTime)/WORKTIME;
        console.log(`Все задачи будут успешно выполнены за ` + Math.ceil(day) + ` дней до наступления дедлайна!`);
    }
    else {
        let hours = -(haveWorkTime - needWorkTime);
        console.log(`Команде разработчиков придется потратить дополнительно ` + Math.ceil(hours) +  ` часов после дедлайна, чтобы выполнить все задачи в беклоге`);
    }


}


let workers = [3, 4, 5, 8];
let tasks = [5, 7, 10, 3, 75];
let deadline = new Date (2020, 2, 26, 20, 59);

calculateProgressWorkers(workers, tasks, deadline);