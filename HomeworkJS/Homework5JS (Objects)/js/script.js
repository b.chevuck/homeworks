/*Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования
* Экранирование дает возможность убрать особое значение у некоторых символов
* Применяется экранирование для отображения/поиска символов в строке
*
**/



let user = createNewUser();

console.log (user);
console.log(user.getAge());
console.log(user.getPassword());




function createNewUser(firstName = prompt("Enter ur name", "Warren"),  lastName = prompt("Enter ur surname", "Buffett"),
                       birthDateString = prompt("Enter ur date of birthday: (dd.mm.yyyy)", "30.08.1930"))
{

    let newUser = {};
    let dob = new Date (birthDateString.substr(6,10),
        birthDateString.substring(3,5)-1,
        birthDateString.substring(0,2));


    Object.defineProperty(newUser, "firstName", {
        value: firstName,
        writable: false,
        configurable: true
    });

    Object.defineProperty(newUser, "lastName", {
        value: lastName,
        writable: false,
        configurable: true
    });

    newUser.setFirstName = function(newFirstName){
        Object.defineProperty(newUser, "firstName", {
            value: newFirstName
        });
    };
    newUser.setLastName = function(newLastName){
        Object.defineProperty(newUser, "lastName", {
            value: newLastName
        });
    };
    newUser.getLogin = function () {
        return (newUser.firstName.substr(0,1) + newUser.lastName).toLowerCase();
    };
    newUser.getAge = function () {
                let today = new Date();
                let age = today.getFullYear() - dob.getFullYear();
                let m = today.getMonth() - dob.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < dob.getDate())) {
                    age--
                }
                return age;
    };
    newUser.getPassword = function () {
            return ((newUser.firstName.substr(0,1)).toUpperCase() + (newUser.lastName).toLowerCase() + dob.getFullYear());
            };


    return newUser;

}

