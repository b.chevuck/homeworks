/*1.Описать своими словами для чего вообще нужны функции в программировании.
2. Описать своими словами, зачем в функцию передавать аргумент.

1. Для того чтобы не дублировать код одних и тех же действий.
2. Значения копируются в локальные переменные и только внутри функции изменяются.

*/

let firstNum = +prompt("Enter first number:");
let secondNum = +prompt("Enter second number:");

while (firstNum === 0 || secondNum === 0){
  firstNum = +prompt("Error. Enter first number:", 10);
  secondNum = +prompt("Error. Enter second number:", 20);
}

let mathSymbol = prompt("Enter basic operation (+ / - *)");

while(mathSymbol !== '+' && mathSymbol !== '/' && mathSymbol !== '*' && mathSymbol !== '-')
{
    mathSymbol = prompt("Error. Enter basic operation (+ / - *)", "+");
}


showOperation(firstNum, secondNum, mathSymbol);





function mathOperation(firstNum, secondNum, mathSymbol) {

    switch (mathSymbol) {
        case '+':
            return firstNum + secondNum;
        case '*':
            return firstNum * secondNum;
        case '-':
            return firstNum - secondNum;
        case '/':
            return firstNum / secondNum;
    }

}

function showOperation(firstNum, secondNum, mathSymbol) {
        console.log("Result of operation " + mathSymbol + " :");
        console.log(firstNum + " " + mathSymbol + " " + secondNum + " = " + mathOperation(firstNum, secondNum, mathSymbol));
}

