/*
Напишите как вы понимаете рекурсию. Для чего она используется на практике?
    Это функция, которая вызывает сама себя
    Нужна когда задачу можно упростить до простых действий.
*/


let firstNum = +prompt("Enter random number:");

while (Number(firstNum) !== firstNum || !firstNum) {
    firstNum = +prompt("Error. Enter random number:");
}


console.log(Factorial(firstNum));


function Factorial(firstNum) {
    if(firstNum <= 1)
    {
        return firstNum;
    }
     return firstNum * Factorial(firstNum-1);

}